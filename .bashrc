# To enable the settings / commands in this file for login shells as well,
# this file has to be sourced in /etc/profile.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, overwrite the one in /etc/profile)
#PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
# only display last 3 parent directory
PS1='\[\033[01;32m\][${PWD#${PWD%/*/*/*}/}]\[\033[00m\]\$ '

# Commented out, don't overwrite xterm -T "title" -n "icontitle" by default.
# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
#    ;;
#*)
#    ;;
#esac

# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# sudo hint
if [ ! -e "$HOME/.sudo_as_admin_successful" ] && [ ! -e "$HOME/.hushlogin" ] ; then
    case " $(groups) " in *\ admin\ *|*\ sudo\ *)
    if [ -x /usr/bin/sudo ]; then
	cat <<-EOF
	To run a command as administrator (user "root"), use "sudo <command>".
	See "man sudo_root" for details.
	
	EOF
    fi
    esac
fi

# if the command-not-found package is installed, use it
if [ -x /usr/lib/command-not-found -o -x /usr/share/command-not-found/command-not-found ]; then
	function command_not_found_handle {
	        # check because c-n-f could've been removed in the meantime
                if [ -x /usr/lib/command-not-found ]; then
		   /usr/lib/command-not-found -- "$1"
                   return $?
                elif [ -x /usr/share/command-not-found/command-not-found ]; then
		   /usr/share/command-not-found/command-not-found -- "$1"
                   return $?
		else
		   printf "%s: command not found\n" "$1" >&2
		   return 127
		fi
	}
fi
cpvi () {
	{
		cp -n .template.c "$1" && vi "$1"
	} ||
	{
		cp -n ~/.template.c "$1" && vi "$1"
	}
}
l () {
		ls -a
}
mkcd () {
		mkdir "$1"
		cd "$1"
	}
alias arm="arm-none-eabi-gdb --command prog_debug_scripts/gdb/openocd_tcp_connect.gdb --command prog_debug_scripts/gdb/load_elf.gdb build/tnc.elf"
alias l="ls -la"
#alias gcc="gcc -Wall"
export PATH="$PATH:/usr/local/MATLAB/R2018b/bin"
export PATH="$PATH:/opt/riscv/bin"
export PATH="$PATH:/opt/qemu/bin"
export PATH="$PATH:/usr/lib/jvm/java-8-openjdk-amd64/bin:/usr/local/bin/hadoop/hadoop-2.10.1/bin:/usr/local/bin/hadoop/hadoop-2.10.1/sbin"
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
export HADOOP_HOME="/usr/local/bin/hadoop/hadoop-2.10.1"
alias cast="cd /home/shivamhire/Desktop/csat/"
alias comm="cd /home/shivamhire/Desktop/csat/comm"
alias oc="cd /home/shivamhire/Desktop/csat/oc"
export DISPLAY=:0
xinput set-int-prop 9 "Device Enabled" 8 1
alias pol="vi /home/shivamhire/Desktop/Computer_Science/parts_of_life"
alias cbd="cd /home/shivamhire/Desktop/COEP/7th_sem/CBD/assignment"
export PATH="$PATH:~/Desktop/COEP/btech_project/outsource_processing/android-studio/bin"
export PIG_HOME="/usr/local/bin/hadoop/pig-0.17.0"
export PATH="$PATH:/usr/local/bin/hadoop/pig-0.17.0/bin"
export PIG_CLASSPATH="/usr/local/bin/hadoop/hadoop-2.10.1/etc/hadoop"

export ANDROID_SDK="~/Android/Sdk"
export PATH=$ANDROID_SDK/emulator:$ANDROID_SDK/tools:$ANDROID_SDK/platform-tools:$PATH

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
